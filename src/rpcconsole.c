#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "libdebug.h"
#include "librpc.h"

static int do_rpc(uint16_t port, uint16_t op, const void* send_data, size_t send_len, FILE* out_fp);

static void print_usage(const char* invoke)
{
    fprintf(stderr,
    "%s --port=<port> --op=<op> [--send --<type>=] [--recv --file=]\n"
    "Valid types:\n"
    "u64, i64, u32, i32, u16, i16, u8, i8, f64, str, file"
    "\n",
    invoke
    );
}

int main(int argc, char** argv)
{
    if(argc < 3) {
        print_usage(argv[0]);
        return -1;
    }
    int send_flag = -1;
    struct option long_options[] = {
        {"send",   no_argument, &send_flag, 1},
        {"recv",   no_argument, &send_flag, 0},
        {"port",   required_argument, NULL, 'y'},
        {"op",     required_argument, NULL, 'z'},
        {"u64",    required_argument, NULL, 'a'},
        {"i64",    required_argument, NULL, 'b'},
        {"u32",    required_argument, NULL, 'c'},
        {"i32",    required_argument, NULL, 'd'},
        {"u16",    required_argument, NULL, 'e'},
        {"i16",    required_argument, NULL, 'f'},
        {"u8",     required_argument, NULL, 'g'},
        {"i8",     required_argument, NULL, 'h'},
        {"f64",    required_argument, NULL, 'i'},
        {"str",    required_argument, NULL, 'j'},
        {"file",   required_argument, NULL, 'k'},
        {0, 0, 0, 0}
    };
    int send_type = -1;
    int32_t op = -1;
    int32_t port = -1;
    void* send_data = NULL;
    size_t send_len = 0;
    char* recv_file = NULL; // if NULL, use stdout
    int opt;
    while((opt = getopt_long(argc, argv, "", long_options, NULL)) != -1) {
        if(opt == -1) break;
        if('a' <= opt && opt <= 'k') {
            if(send_flag == 1) {
                send_type = opt;
            } else if(send_flag != 0) {
                fprintf(stderr, "Missing --send or --recv before type\n");
                print_usage(argv[0]);
                return -1;
            }
        }
        if(send_flag == 0) {
            if(opt == 'k') {
                recv_file = strdup(optarg);
                continue;
            } else if(opt != 0) {
                fprintf(stderr, "--recv must be followed by --file\n");
                print_usage(argv[0]);
                return -1;
            }
        }
        switch(opt) {
        default:
            P_ERR("Unrecognized: %c (%s)", opt, optarg);
            print_usage(argv[0]);
            return -1;
        case 0: break;
        case 'y':
            port = (int32_t)strtoll(optarg, NULL, 0);
        break;
        case 'z':
            op = (int32_t)strtoll(optarg, NULL, 0);
        break;
        case 'a':
            send_len = sizeof(uint64_t);
            send_data = malloc(send_len);
            *(uint64_t*)send_data = strtoull(optarg, NULL, 0);
        break;
        case 'b':
            send_len = sizeof(int64_t);
            send_data = malloc(send_len);
            *(int64_t*)send_data = strtoll(optarg, NULL, 0);
        break;
        case 'c':
            send_len = sizeof(uint32_t);
            send_data = malloc(send_len);
            *(uint32_t*)send_data = (uint32_t)strtoull(optarg, NULL, 0);
        break;
        case 'd':
            send_len = sizeof(int32_t);
            send_data = malloc(send_len);
            *(int32_t*)send_data = (int32_t)strtoll(optarg, NULL, 0);
        break;
        case 'e':
            send_len = sizeof(uint16_t);
            send_data = malloc(send_len);
            *(uint16_t*)send_data = (uint16_t)strtoull(optarg, NULL, 0);
        break;
        case 'f':
            send_len = sizeof(int16_t);
            send_data = malloc(send_len);
            *(int16_t*)send_data = (int16_t)strtoll(optarg, NULL, 0);
        break;
        case 'g':
            send_len = sizeof(uint8_t);
            send_data = malloc(send_len);
            *(uint8_t*)send_data = (uint8_t)strtoull(optarg, NULL, 0);
        break;
        case 'h':
            send_len = sizeof(int8_t);
            send_data = malloc(send_len);
            *(int8_t*)send_data = (int8_t)strtoll(optarg, NULL, 0);
        break;
        case 'i':
            send_len = sizeof(double);
            send_data = malloc(send_len);
            *(double*)send_data = strtod(optarg, NULL);
        break;
        case 'j':
            send_len = strlen(optarg) + 1;
            send_data = strdup(optarg);
        break;
        case 'k':
            send_data = strdup(optarg);
            {
                struct stat sb;
                if(stat(send_data, &sb)) {
                    P_ERR("Failed to stat %s, errno: %d (%s)", optarg, errno, strerror(errno));
                    return -1;
                }
                send_len = (size_t)sb.st_size;
            }
        break;
        }
    }
    if(port == -1 || op == -1 || ((uint32_t)port) > ((uint32_t)UINT16_MAX) || ((uint32_t)port) > ((uint32_t)UINT16_MAX) || (send_type != -1 && !send_data)) {
        P_ERR("send_type: %d, port: %d, op: %d", send_type, port, op);
        print_usage(argv[0]);
        return -1;
    }
    FILE* out_fp = recv_file ? fopen(recv_file, "a") : stdout;
    if(!out_fp) {
        P_ERR("Failed to open: %s, errno: %d (%s)", recv_file, errno, strerror(errno));
        return -1;
    }
    free(recv_file);
    if(send_type == 'k') {
        int fd = open(send_data, O_RDONLY);
        if(fd == -1) {
            P_ERR("Failed to open %s, errno: %d (%s)", (char*)send_data, errno, strerror(errno));
            return -1;
        }
        free(send_data);
        send_data = mmap(NULL, send_len, PROT_READ, MAP_PRIVATE, fd, 0);
        if(send_data == (void*)-1) {
            P_ERR("mmap failed, errno: %d (%s)", errno, strerror(errno));
            return -1;
        }
        close(fd);
    }
    int r = do_rpc((uint16_t)port, (uint16_t)op, send_data, send_len, out_fp);
    if(send_type == 'k') {
        munmap(send_data, send_len);
    } else {
        free(send_data);
    }
    fclose(out_fp);
    return r;
}

#define SECOND (1000 * 1000)

static int do_rpc(uint16_t port, uint16_t op, const void* send_data, size_t send_len, FILE* out_fp)
{
    static char out_data[4096];
    size_t out_len = sizeof(out_data);
    int r = rpc_send_recv(port, op, send_data, send_len, out_data, &out_len, 5*SECOND);
    if(r != EXIT_SUCCESS) {
        P_ERR_STR("rpc action failed");
        return EXIT_FAILURE;
    }
    if(fwrite(out_data, sizeof(char), out_len, out_fp) != out_len) {
        P_ERR("failed to write all result data %zu, errno: %d (%s)", out_len, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

